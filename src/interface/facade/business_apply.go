package facade

import (
	"context"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/business_apply_service/src/application/service"
	"gitlab.com/bishe-projects/business_apply_service/src/interface/assembler"
	"gitlab.com/bishe-projects/common_utils"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/business_apply"
)

var BusinessApplyFacade = new(BusinessApply)

type BusinessApply struct{}

func (f *BusinessApply) GetApplyList(ctx context.Context, req *business_apply.GetApplyListReq) *business_apply.GetApplyListResp {
	resp := &business_apply.GetApplyListResp{}
	applyList, err := service.BusinessApplyApp.GetApplyList(req.ApplicantId, req.Status)
	if err != nil {
		klog.CtxErrorf(ctx, "[BusinessApplyFacade] get apply list failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.ApplyList = assembler.ConvertBusinessApplyEntityListToBusinessApplyList(applyList)
	return resp
}

func (f *BusinessApply) CreateApply(ctx context.Context, req *business_apply.CreateApplyReq) *business_apply.CreateApplyResp {
	resp := &business_apply.CreateApplyResp{}
	uid, err := common_utils.GetUidFromCtx(ctx)
	if err != nil {
		klog.CtxErrorf(ctx, "[BusinessApplyFacade] create apply failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	err = service.BusinessApplyApp.CreateApply(ctx, assembler.ConvertCreateApplyReqToBusinessApplyEntity(req, uid))
	if err != nil {
		klog.CtxErrorf(ctx, "[BusinessApplyFacade] create apply failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
	}
	return resp
}

func (f *BusinessApply) ApproveApply(ctx context.Context, req *business_apply.ApproveApplyReq) *business_apply.ApproveApplyResp {
	resp := &business_apply.ApproveApplyResp{}
	err := service.BusinessApplyApp.ApproveApply(ctx, req.ApplyId)
	if err != nil {
		klog.CtxErrorf(ctx, "[BusinessApplyFacade] approve apply failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
	}
	return resp
}

func (f *BusinessApply) RejectApply(ctx context.Context, req *business_apply.RejectApplyReq) *business_apply.RejectApplyResp {
	resp := &business_apply.RejectApplyResp{}
	err := service.BusinessApplyApp.RejectApply(ctx, req.ApplyId)
	if err != nil {
		klog.CtxErrorf(ctx, "[BusinessApplyFacade] reject apply failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
	}
	return resp
}

func (f *BusinessApply) WithdrawApply(ctx context.Context, req *business_apply.WithdrawApplyReq) *business_apply.WithdrawApplyResp {
	resp := &business_apply.WithdrawApplyResp{}
	err := service.BusinessApplyApp.WithdrawApply(ctx, req.ApplyId)
	if err != nil {
		klog.CtxErrorf(ctx, "[BusinessApplyFacade] withdraw apply failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
	}
	return resp
}

func (f *BusinessApply) AdminApplyList(ctx context.Context, req *business_apply.AdminApplyListReq) *business_apply.AdminApplyListResp {
	resp := &business_apply.AdminApplyListResp{}
	applyList, err := service.BusinessApplyApp.AdminApplyList(ctx, req.Status)
	if err != nil {
		klog.CtxErrorf(ctx, "[BusinessApplyFacade] get admin apply list failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.ApplyList = assembler.ConvertBusinessApplyEntityListToBusinessApplyList(applyList)
	return resp
}

func (f *BusinessApply) OwnApplyList(ctx context.Context, req *business_apply.OwnApplyListReq) *business_apply.OwnApplyListResp {
	resp := &business_apply.OwnApplyListResp{}
	applyList, err := service.BusinessApplyApp.OwnApplyList(ctx, req.Status)
	if err != nil {
		klog.CtxErrorf(ctx, "[BusinessApplyFacade] get own apply list failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.ApplyList = assembler.ConvertBusinessApplyEntityListToBusinessApplyList(applyList)
	return resp
}
