package assembler

import (
	"gitlab.com/bishe-projects/business_apply_service/src/domain/business_apply/entity"
	"gitlab.com/bishe-projects/common_utils/constant/business_constant"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/business_apply"
)

func ConvertCreateApplyReqToBusinessApplyEntity(req *business_apply.CreateApplyReq, uid int64) *entity.BusinessApply {
	return &entity.BusinessApply{
		ApplicantId:  uid,
		BusinessName: req.BusinessName,
		BusinessDesc: req.BusinessDesc,
		Status:       business_constant.ApplyStatusApplying,
	}
}

func ConvertBusinessApplyEntityToBusinessApply(businessApplyEntity *entity.BusinessApply) *business_apply.BusinessApply {
	return &business_apply.BusinessApply{
		Id:            businessApplyEntity.ID,
		ApplicantId:   businessApplyEntity.ApplicantId,
		ApplicantName: businessApplyEntity.ApplicantName,
		BusinessName:  businessApplyEntity.BusinessName,
		BusinessDesc:  businessApplyEntity.BusinessDesc,
		Status:        businessApplyEntity.Status,
	}
}

func ConvertBusinessApplyEntityListToBusinessApplyList(businessApplyEntityList []*entity.BusinessApply) []*business_apply.BusinessApply {
	applyList := make([]*business_apply.BusinessApply, 0, len(businessApplyEntityList))
	for _, businessApplyEntity := range businessApplyEntityList {
		applyList = append(applyList, ConvertBusinessApplyEntityToBusinessApply(businessApplyEntity))
	}
	return applyList
}
