package entity

import (
	"fmt"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/business_apply_service/src/infrastructure/biz_error"
	"gitlab.com/bishe-projects/business_apply_service/src/infrastructure/repo"
	"gitlab.com/bishe-projects/business_apply_service/src/infrastructure/repo/po"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/common_utils/error_enum"
	"gorm.io/gorm"
)

type BusinessApply struct {
	ID            int64
	ApplicantId   int64
	ApplicantName string
	BusinessName  string
	BusinessDesc  string
	Status        int32
}

func (ba *BusinessApply) CreateApply() *business_error.BusinessError {
	err := repo.BusinessApplyMySQLRepo.BusinessApplyDao.CreateApply(ba.ConvertToBusinessApplyPO())
	if err == error_enum.ErrDuplicateEntry {
		return business_error.DataExistsErr
	}
	if err != nil {
		klog.Errorf("[BusinessApplyAggregate] create apply failed: err=%s", err)
		return biz_error.CreateApplyErr
	}
	return nil
}

func (ba *BusinessApply) GetApplyByID(applyId int64) *business_error.BusinessError {
	businessApplyPO, err := repo.BusinessApplyMySQLRepo.BusinessApplyDao.GetApplyByID(applyId)
	if err == gorm.ErrRecordNotFound {
		return business_error.DataNotFoundErr
	}
	if err != nil {
		klog.Errorf("[BusinessApplyAggregate] get apply by id failed, err=%s", err)
		return biz_error.GetApplyByIDErr
	}
	ba.fillBusinessApplyFromBusinessApplyPO(businessApplyPO)
	return nil
}

func (ba *BusinessApply) ChangeStatus(status int32) *business_error.BusinessError {
	err := repo.BusinessApplyMySQLRepo.BusinessApplyDao.UpdateApply(ba.ConvertToBusinessApplyPO(), map[string]interface{}{"status": status})
	if err != nil {
		klog.Errorf("[BusinessApplyAggregate] apply change status failed, err=%s", err)
		return biz_error.ChangeStatusErr
	}
	return nil
}

type BusinessApplyList []*BusinessApply

func (bal *BusinessApplyList) GetApplyList(applicantId *int64, status *int32) *business_error.BusinessError {
	applyPOList, err := repo.BusinessApplyMySQLRepo.BusinessApplyDao.GetApplyList(applicantId, status)
	if err != nil {
		klog.Errorf("[BusinessApplyAggregate] get apply list failed: err=%s")
		return biz_error.GetApplyListErr
	}
	bal.fillBusinessApplyListFromBusinessApplyResultPOList(applyPOList)
	return nil
}

// converter

func (ba *BusinessApply) ConvertToBusinessApplyPO() *po.BusinessApply {
	return &po.BusinessApply{
		ID:           ba.ID,
		ApplicantId:  ba.ApplicantId,
		BusinessName: ba.BusinessName,
		BusinessDesc: ba.BusinessDesc,
		Status:       ba.Status,
	}
}

func (ba *BusinessApply) fillBusinessApplyFromBusinessApplyPO(businessApplyPO *po.BusinessApply) {
	ba.ID = businessApplyPO.ID
	ba.ApplicantId = businessApplyPO.ApplicantId
	ba.BusinessName = businessApplyPO.BusinessName
	ba.BusinessDesc = businessApplyPO.BusinessDesc
	ba.Status = businessApplyPO.Status
}

func (ba *BusinessApply) fillBusinessApplyFromBusinessApplyResultPO(businessApplyResultPO *po.BusinessApplyResult) {
	ba.ID = businessApplyResultPO.ID
	ba.ApplicantId = businessApplyResultPO.ApplicantId
	ba.ApplicantName = businessApplyResultPO.ApplicantName
	ba.BusinessName = businessApplyResultPO.BusinessName
	ba.BusinessDesc = businessApplyResultPO.BusinessDesc
	ba.Status = businessApplyResultPO.Status
}

func (bal *BusinessApplyList) fillBusinessApplyListFromBusinessApplyResultPOList(businessApplyResultList []*po.BusinessApplyResult) {
	*bal = make([]*BusinessApply, 0, len(businessApplyResultList))
	for _, businessApplyResultPO := range businessApplyResultList {
		businessApply := new(BusinessApply)
		businessApply.fillBusinessApplyFromBusinessApplyResultPO(businessApplyResultPO)
		*bal = append(*bal, businessApply)
	}
}

func (ba *BusinessApply) String() string {
	return fmt.Sprintf("id=%d, applicantId=%d, applicantName=%s, businessName=%s, businessDesc=%s, status=%d", ba.ID, ba.ApplicantId, ba.ApplicantName, ba.BusinessName, ba.BusinessDesc, ba.Status)
}
