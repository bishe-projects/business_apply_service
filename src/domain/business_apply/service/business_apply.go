package service

import (
	"gitlab.com/bishe-projects/business_apply_service/src/domain/business_apply/entity"
	"gitlab.com/bishe-projects/common_utils/business_error"
)

var BusinessApplyDomain = new(BusinessApply)

type BusinessApply struct{}

func (d *BusinessApply) CreateApply(apply *entity.BusinessApply) *business_error.BusinessError {
	return apply.CreateApply()
}

func (d *BusinessApply) GetApplyByID(applyId int64) (*entity.BusinessApply, *business_error.BusinessError) {
	apply := new(entity.BusinessApply)
	err := apply.GetApplyByID(applyId)
	if err == business_error.DataNotFoundErr {
		return nil, nil
	}
	return apply, err
}

func (d *BusinessApply) ChangeStatus(apply *entity.BusinessApply, status int32) *business_error.BusinessError {
	return apply.ChangeStatus(status)
}

func (d *BusinessApply) GetApplyList(applicantId *int64, status *int32) ([]*entity.BusinessApply, *business_error.BusinessError) {
	applyList := new(entity.BusinessApplyList)
	err := applyList.GetApplyList(applicantId, status)
	return *applyList, err
}
