package repo

import (
	"gitlab.com/bishe-projects/business_apply_service/src/infrastructure/repo/mysql"
	"gitlab.com/bishe-projects/business_apply_service/src/infrastructure/repo/po"
)

type BusinessApplyDao interface {
	CreateApply(apply *po.BusinessApply) error
	GetApplyByID(int64) (*po.BusinessApply, error)
	GetApplyList(*int64, *int32) ([]*po.BusinessApplyResult, error)
	UpdateApply(*po.BusinessApply, map[string]interface{}) error
}

var BusinessApplyMySQLRepo = NewBusinessApplyRepo(mysql.BusinessApplyMySQLDao)

type BusinessApply struct {
	BusinessApplyDao BusinessApplyDao
}

func NewBusinessApplyRepo(businessApplyDao BusinessApplyDao) *BusinessApply {
	return &BusinessApply{
		BusinessApplyDao: businessApplyDao,
	}
}
