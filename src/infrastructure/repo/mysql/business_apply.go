package mysql

import (
	"gitlab.com/bishe-projects/business_apply_service/src/infrastructure/repo/po"
	"gitlab.com/bishe-projects/common_utils/error_enum"
	"gitlab.com/bishe-projects/common_utils/mysql"
)

var BusinessApplyMySQLDao = new(BusinessApply)

type BusinessApply struct{}

const businessApplyTable = "business_apply"

func (r *BusinessApply) CreateApply(apply *po.BusinessApply) error {
	err := db.Table(businessApplyTable).Create(&apply).Error
	if err != nil {
		if code := mysql.MySqlErrCode(err); code == mysql.ErrDuplicateEntryCode {
			return error_enum.ErrDuplicateEntry
		}
		return err
	}
	return nil
}

func (r *BusinessApply) GetApplyByID(applyId int64) (*po.BusinessApply, error) {
	var apply *po.BusinessApply
	result := db.Table(businessApplyTable).First(&apply, applyId)
	return apply, result.Error
}

func (r *BusinessApply) GetApplyList(applicantId *int64, status *int32) ([]*po.BusinessApplyResult, error) {
	var applyList []*po.BusinessApplyResult
	d := db
	d = d.Table(businessApplyTable).Select("business_apply.id, business_apply.applicant_id, user.name as applicant_name, business_apply.business_name, business_apply.business_desc, business_apply.status").Joins("join user on business_apply.applicant_id = user.id")
	if applicantId != nil {
		d = d.Where("applicant_id = ?", *applicantId)
	}
	if status != nil {
		d = d.Where("status = ?", *status)
	}
	result := d.Order("status").Scan(&applyList)
	return applyList, result.Error
}

func (r *BusinessApply) UpdateApply(apply *po.BusinessApply, values map[string]interface{}) error {
	return db.Table(businessApplyTable).Model(apply).Updates(values).Error
}
