package po

type BusinessApply struct {
	ID           int64
	ApplicantId  int64
	BusinessName string
	BusinessDesc string
	Status       int32
}

type BusinessApplyResult struct {
	ID            int64
	ApplicantId   int64
	ApplicantName string
	BusinessName  string
	BusinessDesc  string
	Status        int32
}
