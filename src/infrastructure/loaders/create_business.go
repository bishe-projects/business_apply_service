package loaders

import (
	"context"
	"errors"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/business_apply_service/src/infrastructure/client"
	"gitlab.com/bishe-projects/common_utils/loader"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/business"
)

type CreateBusinessLoader struct {
	loader.CommonLoader
	// req
	ctx  context.Context
	name string
	desc string
	// resp
	Business *business.Business
}

func (l *CreateBusinessLoader) Load() error {
	resp, err := client.BusinessClient.CreateBusiness(l.ctx, l.newCreateBusinessReq())
	if err == nil && resp.BaseResp != nil && resp.BaseResp.Status < 0 {
		err = errors.New(resp.BaseResp.Message)
	}
	if err != nil {
		klog.CtxErrorf(l.ctx, "[CreateBusinessLoader] create business failed: businessName=%s businessDesc=%s err=%s", l.name, l.desc, err)
		l.SetError(err)
		return err
	}
	l.Business = resp.Business
	return nil
}

func (l *CreateBusinessLoader) newCreateBusinessReq() *business.CreateBusinessReq {
	return &business.CreateBusinessReq{
		Name: l.name,
		Desc: l.desc,
	}
}

func NewCreateBusinessLoader(ctx context.Context, name, desc string) *CreateBusinessLoader {
	return &CreateBusinessLoader{
		ctx:  ctx,
		name: name,
		desc: desc,
	}
}
