package loaders

import (
	"context"
	"errors"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/business_apply_service/src/infrastructure/client"
	"gitlab.com/bishe-projects/common_utils/loader"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/business"
)

type GetBusinessByNameLoader struct {
	loader.CommonLoader
	// req
	ctx          context.Context
	businessName string
	// resp
	Business *business.Business
}

func (l *GetBusinessByNameLoader) Load() error {
	resp, err := client.BusinessClient.GetBusinessByName(l.ctx, l.newGetBusinessByNameReq())
	if err == nil && resp.BaseResp != nil && resp.BaseResp.Status < 0 {
		err = errors.New(resp.BaseResp.Message)
	}
	if err != nil {
		klog.CtxErrorf(l.ctx, "[GetBusinessByNameLoader] get business by name failed: businessName=%s err=%s", l.businessName, err)
		l.SetError(err)
		return err
	}
	l.Business = resp.Business
	return nil
}

func (l *GetBusinessByNameLoader) newGetBusinessByNameReq() *business.GetBusinessByNameReq {
	return &business.GetBusinessByNameReq{
		BusinessName: l.businessName,
	}
}

func NewGetBusinessByNameLoader(ctx context.Context, businessName string) *GetBusinessByNameLoader {
	return &GetBusinessByNameLoader{
		ctx:          ctx,
		businessName: businessName,
	}
}
