package client

import (
	"github.com/cloudwego/kitex/client"
	"github.com/cloudwego/kitex/transport"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/business/businessservice"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/user/userservice"
)

var (
	UserClient     = userservice.MustNewClient("user_service", client.WithHostPorts("0.0.0.0:8881"), client.WithTransportProtocol(transport.TTHeaderFramed))
	BusinessClient = businessservice.MustNewClient("business_service", client.WithHostPorts("0.0.0.0:8882"), client.WithTransportProtocol(transport.TTHeaderFramed))
)
