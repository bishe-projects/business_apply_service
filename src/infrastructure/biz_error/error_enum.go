package biz_error

import "gitlab.com/bishe-projects/common_utils/business_error"

var (
	GetApplyByIDErr       = business_error.NewBusinessError("get apply by id failed", -10001)
	GetApplyListErr       = business_error.NewBusinessError("get apply list failed", -10002)
	ChangeStatusErr       = business_error.NewBusinessError("apply change status failed", -10003)
	ApplyNotExists        = business_error.NewBusinessError("apply not exists", -10004)
	ApplyStatusErr        = business_error.NewBusinessError("the application is not an expected status", -10005)
	NotMyWithdrawErr      = business_error.NewBusinessError("you are not the applicant", -10006)
	HasUserRoleErr        = business_error.NewBusinessError("get user role failed", -10007)
	CreateApplyErr        = business_error.NewBusinessError("create apply failed", -10008)
	GetBusinessByNameErr  = business_error.NewBusinessError("get business by name failed", -10009)
	BusinessNameExistsErr = business_error.NewBusinessError("this business name already exists", -10010)
	ApplyExistsErr        = business_error.NewBusinessError("the application is under review", -10011)
	CreateBusinessErr     = business_error.NewBusinessError("create business failed", -10012)
)
