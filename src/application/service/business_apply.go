package service

import (
	"context"
	"gitlab.com/bishe-projects/business_apply_service/src/domain/business_apply/entity"
	"gitlab.com/bishe-projects/business_apply_service/src/domain/business_apply/service"
	"gitlab.com/bishe-projects/business_apply_service/src/infrastructure/biz_error"
	"gitlab.com/bishe-projects/business_apply_service/src/infrastructure/loaders"
	"gitlab.com/bishe-projects/common_utils"
	"gitlab.com/bishe-projects/common_utils/business_error"
	common_constant "gitlab.com/bishe-projects/common_utils/constant"
	"gitlab.com/bishe-projects/common_utils/constant/business_constant"
)

var BusinessApplyApp = new(BusinessApply)

type BusinessApply struct{}

func (a *BusinessApply) CreateApply(ctx context.Context, apply *entity.BusinessApply) *business_error.BusinessError {
	loader := loaders.NewGetBusinessByNameLoader(ctx, apply.BusinessName)
	if err := loader.Load(); err != nil {
		return biz_error.GetBusinessByNameErr
	}
	if loader.Business != nil {
		return biz_error.BusinessNameExistsErr
	}
	err := service.BusinessApplyDomain.CreateApply(apply)
	if err == business_error.DataExistsErr {
		return biz_error.ApplyExistsErr
	}
	return err
}

func (a *BusinessApply) ApproveApply(ctx context.Context, applyId int64) *business_error.BusinessError {
	has, err := a.isSystemAdmin(ctx)
	if err != nil {
		return err
	}
	if !has {
		return business_error.NoAuthErr
	}
	apply, err := service.BusinessApplyDomain.GetApplyByID(applyId)
	if err != nil {
		return err
	}
	if apply == nil {
		return biz_error.ApplyNotExists
	}
	if apply.Status != business_constant.ApplyStatusApplying {
		return biz_error.ApplyStatusErr
	}
	err = service.BusinessApplyDomain.ChangeStatus(apply, business_constant.ApplyStatusApproved)
	if err != nil {
		return err
	}
	loader := loaders.NewCreateBusinessLoader(ctx, apply.BusinessName, apply.BusinessDesc)
	if loaderErr := loader.Load(); loaderErr != nil {
		return biz_error.CreateBusinessErr
	}
	return nil
}

func (a *BusinessApply) RejectApply(ctx context.Context, applyId int64) *business_error.BusinessError {
	has, err := a.isSystemAdmin(ctx)
	if err != nil {
		return err
	}
	if !has {
		return business_error.NoAuthErr
	}
	apply, err := service.BusinessApplyDomain.GetApplyByID(applyId)
	if err != nil {
		return err
	}
	if apply == nil {
		return biz_error.ApplyNotExists
	}
	if apply.Status != business_constant.ApplyStatusApplying {
		return biz_error.ApplyStatusErr
	}
	return service.BusinessApplyDomain.ChangeStatus(apply, business_constant.ApplyStatusRejected)
}

func (a *BusinessApply) WithdrawApply(ctx context.Context, applyId int64) *business_error.BusinessError {
	uid, err := common_utils.GetUidFromCtx(ctx)
	if err != nil {
		return err
	}
	apply, err := service.BusinessApplyDomain.GetApplyByID(applyId)
	if err != nil {
		return err
	}
	if apply == nil {
		return biz_error.ApplyNotExists
	}
	if apply.ApplicantId != uid {
		return biz_error.NotMyWithdrawErr
	}
	if apply.Status != business_constant.ApplyStatusApplying {
		return biz_error.ApplyStatusErr
	}
	return service.BusinessApplyDomain.ChangeStatus(apply, business_constant.ApplyStatusWithdraw)
}

func (a *BusinessApply) GetApplyList(applicantId *int64, status *int32) ([]*entity.BusinessApply, *business_error.BusinessError) {
	return service.BusinessApplyDomain.GetApplyList(applicantId, status)
}

func (a *BusinessApply) AdminApplyList(ctx context.Context, status *int32) ([]*entity.BusinessApply, *business_error.BusinessError) {
	has, err := a.isSystemAdmin(ctx)
	if err != nil {
		return nil, err
	}
	if !has {
		return nil, business_error.NoAuthErr
	}
	return service.BusinessApplyDomain.GetApplyList(nil, status)
}

func (a *BusinessApply) OwnApplyList(ctx context.Context, status *int32) ([]*entity.BusinessApply, *business_error.BusinessError) {
	uid, err := common_utils.GetUidFromCtx(ctx)
	if err != nil {
		return nil, err
	}
	return service.BusinessApplyDomain.GetApplyList(&uid, status)
}

func (a *BusinessApply) isSystemAdmin(ctx context.Context) (bool, *business_error.BusinessError) {
	uid, businessErr := common_utils.GetUidFromCtx(ctx)
	if businessErr != nil {
		return false, businessErr
	}
	loader := loaders.NewHasUserRoleLoader(ctx, uid, common_constant.SystemAdminRoleID, nil)
	if err := loader.Load(); err != nil {
		return false, biz_error.HasUserRoleErr
	}
	return loader.Has, nil
}
