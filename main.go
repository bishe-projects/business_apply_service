package main

import (
	"github.com/cloudwego/kitex/server"
	business_apply "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/business_apply/businessapplyservice"
	"log"
	"net"
)

func main() {
	addr, _ := net.ResolveTCPAddr("tcp", "127.0.0.1:8883")
	svr := business_apply.NewServer(new(BusinessApplyServiceImpl), server.WithServiceAddr(addr))

	err := svr.Run()

	if err != nil {
		log.Println(err.Error())
	}
}
