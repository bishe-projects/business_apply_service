package main

import (
	"context"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/business_apply_service/src/interface/facade"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/business_apply"
)

// BusinessApplyServiceImpl implements the last service interface defined in the IDL.
type BusinessApplyServiceImpl struct{}

// GetApplyList implements the BusinessApplyServiceImpl interface.
func (s *BusinessApplyServiceImpl) GetApplyList(ctx context.Context, req *business_apply.GetApplyListReq) (resp *business_apply.GetApplyListResp, err error) {
	klog.CtxInfof(ctx, "GetApplyList req=%+v", req)
	resp = facade.BusinessApplyFacade.GetApplyList(ctx, req)
	klog.CtxInfof(ctx, "GetApplyList resp=%+v", resp)
	return
}

// CreateApply implements the BusinessApplyServiceImpl interface.
func (s *BusinessApplyServiceImpl) CreateApply(ctx context.Context, req *business_apply.CreateApplyReq) (resp *business_apply.CreateApplyResp, err error) {
	klog.CtxInfof(ctx, "CreateApply req=%+v", req)
	resp = facade.BusinessApplyFacade.CreateApply(ctx, req)
	klog.CtxInfof(ctx, "CreateApply resp=%+v", resp)
	return
}

// ApproveApply implements the BusinessApplyServiceImpl interface.
func (s *BusinessApplyServiceImpl) ApproveApply(ctx context.Context, req *business_apply.ApproveApplyReq) (resp *business_apply.ApproveApplyResp, err error) {
	klog.CtxInfof(ctx, "ApproveApply req=%+v", req)
	resp = facade.BusinessApplyFacade.ApproveApply(ctx, req)
	klog.CtxInfof(ctx, "ApproveApply resp=%+v", resp)
	return
}

// RejectApply implements the BusinessApplyServiceImpl interface.
func (s *BusinessApplyServiceImpl) RejectApply(ctx context.Context, req *business_apply.RejectApplyReq) (resp *business_apply.RejectApplyResp, err error) {
	klog.CtxInfof(ctx, "RejectApply req=%+v", req)
	resp = facade.BusinessApplyFacade.RejectApply(ctx, req)
	klog.CtxInfof(ctx, "RejectApply resp=%+v", resp)
	return
}

// WithdrawApply implements the BusinessApplyServiceImpl interface.
func (s *BusinessApplyServiceImpl) WithdrawApply(ctx context.Context, req *business_apply.WithdrawApplyReq) (resp *business_apply.WithdrawApplyResp, err error) {
	klog.CtxInfof(ctx, "WithdrawApply req=%+v", req)
	resp = facade.BusinessApplyFacade.WithdrawApply(ctx, req)
	klog.CtxInfof(ctx, "WithdrawApply resp=%+v", resp)
	return
}

// AdminApplyList implements the BusinessApplyServiceImpl interface.
func (s *BusinessApplyServiceImpl) AdminApplyList(ctx context.Context, req *business_apply.AdminApplyListReq) (resp *business_apply.AdminApplyListResp, err error) {
	klog.CtxInfof(ctx, "AdminApplyList req=%+v", req)
	resp = facade.BusinessApplyFacade.AdminApplyList(ctx, req)
	klog.CtxInfof(ctx, "AdminApplyList resp=%+v", resp)
	return
}

// OwnApplyList implements the BusinessApplyServiceImpl interface.
func (s *BusinessApplyServiceImpl) OwnApplyList(ctx context.Context, req *business_apply.OwnApplyListReq) (resp *business_apply.OwnApplyListResp, err error) {
	klog.CtxInfof(ctx, "OwnApplyList req=%+v", req)
	resp = facade.BusinessApplyFacade.OwnApplyList(ctx, req)
	klog.CtxInfof(ctx, "OwnApplyList resp=%+v", resp)
	return
}
